// swift-tools-version:4.0
import PackageDescription

let package = Package(
    name: "employee",
    dependencies: [
        // 💧 A server-side Swift web framework.
        .package(url: "https://github.com/vapor/vapor.git", from: "3.0.0"),
        .package(url: "https://github.com/vapor/fluent-mysql.git", from: "3.0.0"),
        .package(url: "https://github.com/vapor/leaf.git", from: "3.0.0"),
        .package(url: "https://github.com/vapor/url-encoded-form.git", from: "1.0.0"),
        .package(url: "https://github.com/vapor/auth.git", from: "2.0.0-rc"),
        .package(url: "https://github.com/vapor-community/leaf-markdown.git", from: "2.0.0"),
        .package(url: "https://github.com/twof/VaporMailgunService.git", from: "1.5.0"),
        .package(url: "https://github.com/vapor-community/sendgrid-provider.git", from: "3.0.0")
    ],
    targets: [
        .target(name: "App", dependencies: ["FluentMySQL", "Vapor","URLEncodedForm","Leaf", "Authentication","LeafMarkdown","Mailgun","SendGrid"]),
        .target(name: "Run", dependencies: ["App"]),
        .testTarget(name: "AppTests", dependencies: ["App"])
]

)
