import App
import XCTest
import Vapor


final class AppTests: XCTestCase {
    func testNothing() throws {
        // Add your tests here
        XCTAssert(true)
    }

    
    func testSomething() throws {
        let app = try Application()
        let client = try app.make(Client.self)
        let res = try client.send(.GET, to: "https://itunes.apple.com/search?term=mapstr&country=fr&entity=software&limit=1").wait()
        let data = res.http.body.data ?? Data()
        XCTAssertEqual(String(data: data, encoding: .ascii)?.contains("iPhone"), true)
    }
    
    

    
    
    static let allTests = [
        ("testNothing", testNothing)
    ]
}
