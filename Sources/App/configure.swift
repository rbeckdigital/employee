import FluentMySQL
import Vapor
import Leaf
import Authentication
import LeafMarkdown
import Mailgun
import SendGrid


/// Called before your application initializes.
public func configure(_ config: inout Config, _ env: inout Environment, _ services: inout Services) throws {

    // Register providers first
    try services.register(FluentMySQLProvider())
    try services.register(LeafProvider())
    try services.register(AuthenticationProvider())
    try services.register(SendGridProvider())
    
    
    /// Preferences
    config.prefer(MemoryKeyedCache.self, for: KeyedCache.self)
    config.prefer(LeafRenderer.self, for: ViewRenderer.self)
    
    // Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)
    
    /// Leaf Markdown
    var tags = LeafTagConfig.default()
    tags.use(Markdown(), as: "markdown")
    tags.use(uidate(), as: "uidate")
    tags.use(uitags(), as: "uitags")
    tags.use(uidue(), as: "uidue")
    services.register(tags)
    
    
    // Register middleware
    var middlewares = MiddlewareConfig() // Create _empty_ middleware config
    // middlewares.use(FileMiddleware.self) // Serves files from `Public/` directory
    middlewares.use(ErrorMiddleware.self) // Catches errors and converts to HTTP response
    middlewares.use(SessionsMiddleware.self)
    middlewares.use(FileMiddleware.self)
    services.register(middlewares)
    
    // Configure a MySQL database
    if env.isRelease {
        print("set production access")
        let mysqlConfig = MySQLDatabaseConfig(
            hostname: "127.0.0.1",
            port: 3306,
            username: "remoteuser",
            password: "AkNK7MZa",
            database: "employee"
        )
        services.register(mysqlConfig)
    } else {
        print("set local access")
        let mysqlConfig = MySQLDatabaseConfig(
            hostname: "127.0.0.1",
            port: 3306,
            username: "employee",
            password: "employee",
            database: "employee"
        )
        services.register(mysqlConfig)
    }

    
    // MailGun
    let mailgun = Mailgun(apiKey: "b0a4064af88ed312d3a38d6e48635676-7caa9475-60d48fd2", domain: "team.digital-possibilites.com")
    services.register(mailgun, as: Mailgun.self)
    
    // SendGrid
    let sendgridconfig = SendGridConfig(apiKey: "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")
    services.register(sendgridconfig)
  
    
    // Configure migrations
    var migrations = MigrationConfig()
    migrations.add(model: User.self, database: .mysql)
    migrations.add(model: UserDetails.self, database: .mysql)
    migrations.add(model: ListItem.self, database: .mysql)
    migrations.add(model: Employee.self, database: .mysql)
    migrations.add(model: Commenting.self, database: .mysql)
    migrations.add(model: EmployeeUser.self, database: .mysql)
    migrations.add(model: ListType.self, database: .mysql)
    migrations.add(model: ListColumn.self, database: .mysql)
    migrations.add(model: ChangePassword.self, database: .mysql)
    migrations.add(model: Share.self, database: .mysql)
    services.register(migrations)
}
