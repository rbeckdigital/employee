//
//  extensions.swift
//  App
//
//  Created by Richard Beck on 2019-02-26.
//

import Foundation
import Vapor


struct ArrayContext<T: Content>: Content {
    let items: [T]
}

extension Sequence where Element: Content {
    func asArrayContext() -> ArrayContext<Element> {
        return ArrayContext(items: Array(self))
    }
}

extension Future {
    func asArrayContext<T>() -> Future<ArrayContext<T>> where T: Content, Expectation == [T] {
        return map { $0.asArrayContext() }
    }
}


extension Date {
    
    func toEst() -> Date {
        
        return self+(Double(TimeZone(identifier: "EST")?.secondsFromGMT() ?? 0))
    }
    
    
}


extension String {
    var isValidURL: Bool {
        
        if self.prefix(4) == "http" {
            return true
        } else {
            return false
        }
    }
}




