//
//  LeafExtension.swift
//  App
//
//  Created by Richard Beck on 2019-03-07.
//

import Foundation
import Vapor



final class uidate: TagRenderer {
    init() { }
    
    func render(tag: TagContext) throws -> EventLoopFuture<TemplateData> {
        let formatter = DateFormatter()
   
        if tag.parameters[0].double == nil {return tag.container.future(.string(""))}
        
        let dateInDateFormat = Date(timeIntervalSince1970: tag.parameters[0].double!)
        
        formatter.dateFormat = "yyyy-MM-dd"
        
        var returnString = formatter.string(from: dateInDateFormat)
        
        let timeDifference = -dateInDateFormat.timeIntervalSinceNow
        
        // Past
        
        if timeDifference >= 0 && timeDifference <= 60 {
            returnString = "1 minute ago"
        }
        
        if timeDifference > 60 && timeDifference <= 3600 {
            returnString = "\(Int(timeDifference/60)) minutes ago"
        }
        
        if timeDifference > 3060 && timeDifference <= 86400 {
            let periods = Int(timeDifference/3600)
            var ending = ""
            if periods>1 {ending="s"}
            returnString = "\(periods) hour\(ending) ago"
        }
        

        if timeDifference > 86400 && timeDifference < 604800 {
            let periods = Int(timeDifference/86400)
            var ending = ""
            if periods>1 {ending="s"}
            returnString = "\(periods) day\(ending) ago"
        }
        
        // Future
        
        if -timeDifference > 60 && -timeDifference <= 3600 {
            let periods = Int(-timeDifference/3600)
            var ending = ""
            if periods>1 {ending="s"}
            returnString = "In \(periods) minute\(ending)"
        }
        
        if -timeDifference > 3060 && -timeDifference <= 86400 {
            let periods = Int(-timeDifference/3600)
            var ending = ""
            if periods>1 {ending="s"}
            returnString = "In \(periods) hour\(ending)"
        }
        
        if -timeDifference > 86400 && -timeDifference < 604800 {
            let periods = Int(-timeDifference/86400)
            var ending = ""
            if periods>1 {ending="s"}
            returnString = "In \(periods) day\(ending)"
        }
        


        return tag.container.future(.string(returnString))
    }
    
    
}

final class uitags: TagRenderer {
    init() { }
    
    func render(tag: TagContext) throws -> EventLoopFuture<TemplateData> {
       
        var returnString:String = ""
        var style = "pill"
        
        let listOfTags = tag.parameters[0].string?.split(separator: ",")
        
        switch tag.parameters[1].string {
            case "small": style = "pill-small"
            default: style = "pill"
        }
        
        for tag in listOfTags ?? [] {
            returnString += "<span class='\(style)'>\(tag)</span>"
        }
        
        
        return tag.container.future(.string(returnString))
    }
    
    
}



final class uidue: TagRenderer {
    init() { }
    
    func render(tag: TagContext) throws -> EventLoopFuture<TemplateData> {
        
        if tag.parameters[0].double == nil {return tag.container.future(.string(""))}
        
        let dateInDateFormat = Date(timeIntervalSince1970: tag.parameters[0].double!)
        
        let timeDifference = -dateInDateFormat.timeIntervalSinceNow
        
        var returnString = "Today"
        
        if timeDifference > 86400 {
            let periods = Int(timeDifference/86400)
            var ending = ""
            if periods>1 {ending="s"}
            returnString = "<span class='dueDateAgo'>\(periods) day\(ending) ago</span>"}
        
        if -timeDifference > 86400 {
            let periods = Int(-timeDifference/86400)
            var ending = ""
            if periods>1 {ending="s"}
            returnString = "<span class='dueDate'>In \(periods) day\(ending)</span>"}
        
        return tag.container.future(.string(returnString))
    }
    
    
}
