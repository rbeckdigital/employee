//
//  UIControllerEmployeeMgmt.swift
//  App
//
//  Created by Richard Beck on 2019-03-01.
//

import Foundation
import Vapor
import Fluent
import FluentMySQL


final class UIControllerEmployeeManagement {

    func renderEmployees(_ req: Request) throws -> Future<View> {
        let userId = Int(try req.session()["user"] ?? "0")
        return Employee.query( on:req)
            .join(\EmployeeUser.employeeId, to: \Employee.id)
            .filter(\EmployeeUser.userId == userId!)
            .sort(\.name, .ascending).all().flatMap { employees in
                let data = ["employeeList":employees]
                return try req.view().render("employeeManagement/employeeMgmtList", data)
        }
    }

    
    func renderEditEmployee(_ req: Request) throws -> Future<View> {
        return try Employee.find(req.parameters.next(Int.self), on:req).flatMap { employee in
            if employee == nil {throw Abort(.notFound)}
            let data = ["employee":employee!]
            return try req.view().render("employeeManagement/employeeMgmtUpdateForm", data)
        }
    }
    
    func renderNewEmployee(_ req: Request) throws -> Future<View> {
        return try req.view().render("employeeManagement/employeeMgmtUpdateForm" )
        
    }
    
    
}
