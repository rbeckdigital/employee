//
//  UIController.swift
//  App
//
//  Created by Richard Beck on 2019-02-19.
//

import Foundation
import Vapor
import Fluent
import FluentMySQL


final class UIController {
    
    func renderLogin(_ req: Request) throws -> Future<View> {
        return try req.view().render("login")
    }
    
    func renderRegister(_ req:Request) throws -> Future<View> {
        return try req.view().render("register")
        
    }
    
    
    func renderHome(_ req: Request) throws -> Future<View> {
        return try req.view().render("home")
    }

    func renderTaskList(_ req: Request) throws -> Future<View> {
        return try req.view().render("taskList")
    }
    
    func renderItems(_ req: Request) throws -> Future<View> {
        let listType = try req.parameters.next(String.self)
        let employee = try req.parameters.next(Int.self)
        guard let userId = Int(try req.session()["user"] ?? "0") else { throw Abort(.badRequest) }
        return ListItem.query(on: req)
            .filter(\.type == listType)
            .filter(\.employeeId == employee)
            .filter(\.status == "open")
            .filter(\.userId == userId)
            .sort(\.order, .ascending)
            .all().flatMap {items in
            let data = ["items":items]
            return try req.view().render("itemList", data)
        }
        
        
    }
    
    struct ItemAndEmployees:Encodable {
        var item:ListItem
        var employees: Future<[Employee]>
        var types: Future<[ListType]>
    }
    
    func renderItemInfo(_ req: Request) throws -> Future<View> {
        return try ListItem.find(req.parameters.next(Int.self), on:req).flatMap { item in
            if item == nil {throw Abort(.notFound)}
            let employees = try self.getEmployees(req)
            let types = try self.getLists(req)
            let data = ["itemAndEmployees":ItemAndEmployees(item:item!,employees:employees, types:types)]
            return try req.view().render("itemInfo", data)
        }
    }
    
    func getEmployees(_ req:Request) throws -> Future<[Employee]> {
        let userId = Int(try req.session()["user"] ?? "0")
        return Employee.query(on:req)
                .join(\EmployeeUser.employeeId, to: \Employee.id)
                .filter(\EmployeeUser.userId == userId!)
                .sort(\.name, .ascending)
                .all()
                .map {$0}
    }
    
    
    func getLists(_ req:Request) throws -> Future<[ListType]> {
        let userId = Int(try req.session()["user"] ?? "0")
        return ListType.query(on:req)
            .filter(\.userId == userId!)
            .sort(\.name, .ascending)
            .all()
            .map {$0}
    }
    

    func renderItemReadOnly(_ req: Request) throws -> Future<View> {
        return try ListItem.find(req.parameters.next(Int.self), on:req).flatMap { item in
            if item == nil {throw Abort(.notFound)}
            let showItem = ShowItem.init(id: item?.id,
                                         userId: (item?.userId)!,
                                         type: try self.getListType(req, typeCode:(item?.type)!),
                                         title: (item?.title)!,
                                         description: item?.description,
                                         dueDate: (item?.dueDate),
                                         order: (item?.order)!,
                                         employeeName: try self.getEmployeeName(req, employeeId: (item?.employeeId)!),
                                         externalReference: item?.externalReference,
                                         externalReferenceType: (item?.externalReference ?? "").isValidURL,
                                         status: (item?.status)!,
                                         labels: self.getTags(input: item?.labels),
                                         createdAt: item?.createdAt,
                                         updatedAt: item?.updatedAt
            )
            let data = ["item":showItem]
            return try req.view().render("itemReadOnly", data)
        }
    }
    
    
    func getEmployeeName(_ req:Request, employeeId:Int) throws ->Future<String> {
        
        if employeeId == 0 {
            return req.future("personalTaskList")
        }
        
        return Employee.find(employeeId, on:req).unwrap(or: Abort(.badRequest)).map {employee in return employee.name}
    }
    
    func getListType(_ req:Request, typeCode:String) throws ->Future<String> {
        let userId = Int(try req.session()["user"] ?? "0")
        return ListType.query(on:req)
            .filter(\.typeCode == typeCode)
            .filter(\.userId == userId!)
            .first()
            .map {type in (type?.name)!}
    }
    
    func getTags(input:String?) -> [String] {
        var returnArray = [String]()
        
        let separated = (input ?? "").split(separator: ",")
        
        for item in separated {
            returnArray.append(String(item))
        }
        return returnArray
    }
    
    
    
    func renderEditComment(_ req: Request) throws -> Future<View> {
        return try Commenting.find(req.parameters.next(Int.self), on:req).flatMap { comment in
            if comment == nil {throw Abort(.notFound)}
            let data = ["comment":comment!]
            return try req.view().render("components/editComment", data)
        }
    }
    
    
    func renderItemNew(_ req: Request) throws -> Future<View> {
            let itemType = try req.parameters.next(String.self)
            let employeeId = try req.parameters.next(Int.self)
            if itemType.count == 0 {throw Abort(.badRequest)}
            let data = ["itemType":itemType, "employeeId":String(employeeId)]
        return try req.view().render("itemInfo", data)
        
    }
 
    
    func renderEmployees(_ req: Request) throws -> Future<View> {
        let userId = Int(try req.session()["user"] ?? "0")
        return Employee.query( on:req)
                   .join(\EmployeeUser.employeeId, to: \Employee.id)
                    .filter(\EmployeeUser.userId == userId!)
                    .sort(\.name, .ascending).all().flatMap { employees in
                            let data = ["employees":employees]
                            return try req.view().render("components/employees", data)
        }
    }
    
    



    func renderOverview(_ req: Request) throws -> Future<View> {
        
        let userId = Int(try req.session()["user"] ?? "0")
        
        func commentsForItem(_ item: ListItem, on req: Request) -> Future<ListItemAndComments> {
            return Commenting.query(on: req)
                .filter(\.listItemId == item.id!)
                .sort(\.createdAt, .descending)
                .range(..<5).all()  // F<[C]>
                .map { ListItemAndComments(item: item, commentList: $0) } // F<[C]> -> F<LIAC>
        }
        
        func openItemsForEmployee(_ employee: Employee, on req: Request) -> Future<EmployeeListItemAndComments> {
            return ListItem.query(on: req)
                .filter(\.userId == userId!)
                .filter(\.employeeId == employee.id!)
                .group(.or) {
                    $0.filter(\.type == "roadmap").filter(\.type == "backlog")
                }
                .filter(\.status == "open").all() // F<[I]>
                .flatMap { listItems in
                    listItems.map { commentsForItem($0, on: req) } // [I] -> [F<LIAC>]
                        .flatten(on: req) // [F<LIAC>] -> F<[LIAC]>
                        .map { EmployeeListItemAndComments(employee: employee, listItemAndComments: $0) } // F<[LIAC]> -> F<ELIAC>
            }
        }
        
        return Employee.query(on:req)
            .join(\EmployeeUser.employeeId, to: \Employee.id)
            .filter(\EmployeeUser.userId == userId ?? 0)
            .sort(\.name, .ascending).all()
            .flatMap { employees -> Future<View> in
                employees.map { openItemsForEmployee($0, on: req) } // [E] -> [F<ELIAC>]
                    .flatten(on: req) // [F<ELIAC>] -> F<[ELIAC]>
                    .flatMap {
                        return try req.view().render("overview", ["EA": $0]) } // F<[ELIAC]> -> F<View>
        }
    }

    
    
    
    func renderComments(_ req: Request) throws -> Future<View> {
        let listItemIdp = try req.parameters.next(Int.self)
        return Commenting.query( on:req).filter(\.listItemId == listItemIdp).sort(\.createdAt, .descending).all().flatMap { comments in
            let data = ["comments":comments]
            return try req.view().render("components/comments", data)
        }
    }
    
            
 

}



