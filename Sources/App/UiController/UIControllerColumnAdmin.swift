//
//  UIControllerColumnAdmin.swift
//  App
//
//  Created by Richard Beck on 2019-02-28.
//

import Foundation
import Vapor
import Fluent
import FluentMySQL


final class UIControllerColumnAdmin {


    func renderColumnAdministration(_ req: Request) throws -> Future<View> {
        let userId = Int(try req.session()["user"] ?? "0")
        return ListColumn.query(on: req)
                        .filter(\.userId == userId!)
                        .all()
                        .flatMap {
                            return try req.view().render("columnsAdministration",["cols":$0])
        } }

    
    
    func renderEditColumn(_ req: Request) throws -> Future<View> {
        return try ListColumn.find(req.parameters.next(Int.self), on:req).flatMap { column in
            if column == nil {throw Abort(.notFound)}
            let data = ["column":column!]
            return try req.view().render("columnAdminUpdateForm", data)
        }
    }
    
    func renderNewColumn(_ req: Request) throws -> Future<View> {
            return try req.view().render("columnAdminUpdateForm" )
        
    }
    
    struct listsAndColumn: Encodable {
        var column:Int
        var lists:[ListType]
    }
    
    func renderListAdministration(_ req: Request) throws -> Future<View> {
        let userId = Int(try req.session()["user"] ?? "0")
        let column = try req.parameters.next(Int.self)
        return ListType.query(on: req)
            .filter(\.userId == userId!)
            .filter(\ListType.column == column)
            .all()
            .flatMap { lists in
                let data = ["listsAndColumn":listsAndColumn(column: column, lists:lists)]
                return try req.view().render("listAdmin/listAdmin",data)
        } }
    
    
    
    func renderEditList(_ req: Request) throws -> Future<View> {
        return try ListType.find(req.parameters.next(Int.self), on:req).flatMap { list in
            if list == nil {throw Abort(.notFound)}
            let data = ["list":list!]
            return try req.view().render("listAdmin/listAdminUpdateForm", data)
        }
    }
    
    func renderNewList(_ req: Request) throws -> Future<View> {
        let column = try req.parameters.next(Int.self)
        return try req.view().render("listAdmin/listAdminUpdateForm", ["column":column] )
        
    }
    
    
    
}
