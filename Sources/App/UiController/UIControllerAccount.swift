//
//  UIControllerAccount.swift
//  App
//
//  Created by Richard Beck on 2019-03-02.
//

import Foundation
import Vapor
import Fluent
import FluentMySQL


final class UIControllerAccount {
    
    struct accountDetails: Encodable {
        var user: Future<User>
        var userDetails: Future<UserDetails>
    }
    
    func renderAccount(_ req: Request) throws -> Future<View> {
        let userId = Int(try req.session()["user"] ?? "0")
        let data = ["account":accountDetails(user:try getuser(req, userId:userId!) ,userDetails:try getuserDetails(req, userId:userId!))]
        return try req.view().render("account", data )
    }
    

    func getuser(_ req:Request, userId:Int) throws -> Future<User> {
        return User.find(userId, on:req).map {user in
            if user == nil {throw Abort(.notFound)}
            return user!
        }
        
    }
    
    func getuserDetails(_ req:Request, userId:Int) throws -> Future<UserDetails> {
        return UserDetails.query(on:req)
                .filter(\.userId == userId)
                .first()
                .flatMap {details in
                    let ud = UserDetails(id: details?.id, userId: userId, name: details?.name)
                    return ud.save(on: req)
                    
        }
        }
    
}


