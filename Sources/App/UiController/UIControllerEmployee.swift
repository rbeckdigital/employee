//
//  UIControllerEmployee.swift
//  App
//
//  Created by Richard Beck on 2019-02-28.
//

import Foundation
import Vapor
import Fluent
import FluentMySQL



final class UIControllerEmployee {

    struct colsType: Encodable {
        var column:Int
        var columnDetails: Future<[ListColumn]>?
        var types: Future<[ListType]>
    }
    
    struct EmployeeScreen: Encodable {
        
        var employee: Employee
        var employeeList: Future<[Employee]>
        var typeList: Future<[colsType]>
    }
    
    func renderEmployeeAPI(_ req: Request) throws -> Future<EmployeeScreen> {
        let employeeId = try req.parameters.next(Int.self)
        
        return Employee.find(employeeId, on:req).flatMap {employee in
            if employee == nil {throw Abort(.notFound)}
            let employeeList = try self.getEmployeeList(req)
            let typeList = try self.getTypeList(req)
            
            return req.future(EmployeeScreen(employee:employee!, employeeList:employeeList, typeList:typeList))
            
        }
    }
    
    
    
    
    func renderEmployee(_ req: Request) throws -> Future<View> {
        let employeeId = try req.parameters.next(Int.self)
    
        return Employee.find(employeeId, on:req).flatMap {employee in
            if employee == nil {throw Abort(.notFound)}
            let employeeList = try self.getEmployeeList(req)
            let typeList = try self.getTypeList(req)
            
            return try req.view().render("employee", EmployeeScreen(employee:employee!, employeeList:employeeList, typeList:typeList))
            
        }
    }
    
    
    
    func getEmployeeList(_ req: Request) throws -> Future<[Employee]> {
        let userId = Int(try req.session()["user"] ?? "0")
        return Employee.query(on: req)
            .join(\EmployeeUser.employeeId, to: \Employee.id)
            .filter(\EmployeeUser.userId == userId ?? 0)
            .sort(\Employee.createdAt, .descending)
            .all() // F<[C]> -> F<LIAC>
    }
    
    
    func getTypeList(_ req: Request) throws -> Future<[colsType]> {
        let userId = Int(try req.session()["user"] ?? "0")
        
        var cols = Set<Int>()
        
        return try self.getTypeColumns(req).map {columns in try columns.map { column in
           
            if column != nil {
                cols.insert(column!)
                
                let columnDetails = try self.getColumnDefinition(req, column: column!, userId: userId!)
                
                
                return colsType(column:column!, columnDetails:columnDetails, types: try self.getTypePerColumn(req, column:column!, userId:userId!))
                
                }
            return colsType(column:0, columnDetails:nil, types: try self.getTypePerColumn(req, column:0, userId:userId!))
            
            }
            
            }

    }
    
    
    
    func getTypePerColumn(_ req:Request, column:Int, userId:Int) throws -> Future<[ListType]> {
    return ListType.query(on: req)
            .filter(\ListType.userId == userId)
            .filter(\ListType.column == column)
            .sort(\ListType.order, .ascending)
            .all().map { types in types}
    }
    
    func getTypeColumns(_ req: Request) throws -> Future<[Int?]> {
        let userId = Int(try req.session()["user"] ?? "0")
        var distinctCol = Set<Int>()
        return ListType.query(on: req)
                .filter(\ListType.userId, .equal, userId!)
                .sort(\ListType.column, .ascending)
                .all().map {types in
                    types.map { type -> Int? in
                        if distinctCol.contains(type.column) {
                            return nil
                        } else {
                            distinctCol.insert(type.column)
                            return type.column
                        }
                        
                    }

    } }
 
    func getColumnDefinition(_ req: Request, column:Int, userId:Int) throws -> Future<[ListColumn]> {
        return ListColumn.query(on: req)
               .filter(\.userId == userId)
               .filter(\.id == column)
               .all()
    }
    
}

