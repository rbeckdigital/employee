//
//  employee.swift
//  App
//
//  Created by Richard Beck on 2019-02-19.
//

import Foundation
import FluentMySQL
import Vapor


final class Employee: MySQLModel {
    
      typealias Database = MySQLDatabase
    
    var id: Int?
    var name: String
    var photo: String?
    var background: String?
    var email: String?
    var createdAt: Date?
    var updatedAt: Date?
    
    
    init(id: Int?, name:String, photo:String?, background:String?, email:String?) {
        self.id = id
        self.name = name
        self.photo = photo
        self.background = background
        self.email = email
    
    
}

}


extension Employee: Content {}
extension Employee: Migration {}
extension Employee {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension Employee: Parameter {}
