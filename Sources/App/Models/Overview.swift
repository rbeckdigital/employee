//
//  Overview.swift
//  App
//
//  Created by Richard Beck on 2019-02-25.
//

import Foundation
import FluentMySQL
import Vapor


struct ListItemAndComments:Content {
      typealias Database = MySQLDatabase
    
    var item: ListItem?
    var commentList: [Commenting]?
    
    init (item:ListItem?, commentList:[Commenting]?) {
        self.item = item
        self.commentList = commentList
    }
}


struct EmployeeListItemAndComments:Content {
    var employee: Employee
    var listItemAndComments: [ListItemAndComments]?
    
    init (employee:Employee, listItemAndComments: [ListItemAndComments]?) {
        self.employee = employee
        self.listItemAndComments = listItemAndComments
    }
}


struct EmployeeActivites:Content {
    var list: [EmployeeListItemAndComments]?
    
    init (list:[EmployeeListItemAndComments]?) {
        self.list = list
    }
}
