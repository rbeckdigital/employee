//
//  Comment.swift
//  App
//
//  Created by Richard Beck on 2019-02-21.
//

import Foundation
import FluentMySQL
import Vapor


final class Commenting: MySQLModel {
    
      typealias Database = MySQLDatabase
    
    
    var id: Int?
    var userId: Int
    var listItemId: Int
    var comment: String
    var createdAt: Date?
    var updatedAt: Date?
    
    init(id: Int?, userId:Int, listItemId: Int, comment:String) {
        self.id = id
        self.userId = userId
        self.listItemId = listItemId
        self.comment = comment


    }
    
    
}


extension Commenting: Content {}
extension Commenting: Migration {}
extension Commenting {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension Commenting: Parameter {}



struct CommentingDisplay: Encodable {
    var id: Int?
    var userId: Int
    var listItemId: Int
    var comment: String
    var createdAt: Date?
    var updatedAt: Date?
    
    init(commenting: Commenting) {
        self.id = commenting.id
        self.userId = commenting.userId
        self.listItemId = commenting.listItemId
        self.comment = commenting.comment
        self.createdAt = commenting.createdAt
        self.updatedAt = commenting.updatedAt
        
    }
    
    
}
