//
//  ListColumn.swift
//  App
//
//  Created by Richard Beck on 2019-02-28.
//

import Foundation
import FluentMySQL
import Vapor


final class ListColumn: MySQLModel {
      typealias Database = MySQLDatabase
    
    var id: Int?
    var column:Int
    var userId: Int
    var name: String?
    var visibility: String?
    var createdAt: Date?
    var updatedAt: Date?
    
    init(id: Int?, column:Int, userId:Int, name:String, visibility: String) {
        self.column = column
        self.userId = userId
        self.name = name
        self.visibility = visibility
        
    }
    
    
}


extension ListColumn: Content {}
extension ListColumn: Migration {}
extension ListColumn {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension ListColumn: Parameter {}

