//
//  UserDetails.swift
//  App
//
//  Created by Richard Beck on 2019-02-19.
//

import Foundation
import FluentMySQL
import Vapor


final class UserDetails: MySQLModel {
      typealias Database = MySQLDatabase
    
    var id: Int?
    var userId: Int
    var name: String?
    var createdAt: Date?
    var updatedAt: Date?

    
    init(id: Int?, userId:Int, name: String?) {
        self.id = id
        self.userId = userId
        self.name = name


        
    }
    
    
}


extension UserDetails: Content {}
extension UserDetails: Migration {}
extension UserDetails {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension UserDetails: Parameter {}


extension User {
    // this galaxy's related planets
    var userdetails: Children<User, UserDetails> {
        return children(\.userId)
    }
}

extension UserDetails {
    // this planet's related galaxy
    var user: Parent<UserDetails, User> {
        return parent(\.userId)
    }
}
