//
//  ListType.swift
//  App
//
//  Created by Richard Beck on 2019-02-28.
//

import Foundation
import FluentMySQL
import Vapor


final class ListType: MySQLModel {
      typealias Database = MySQLDatabase
    
    var id: Int?
    var typeCode: String
    var name: String
    var userId: Int
    var column: Int
    var order: Int
    var createdAt: Date?
    var updatedAt: Date?
    
    init(id: Int?, typeCode:String, name: String, userId:Int, column:Int, order:Int) {
        self.typeCode = typeCode
        self.name = name
        self.userId = userId
        self.column = column
        self.order = order
        
        
    }
    
    
}


extension ListType: Content {}
extension ListType: Migration {}
extension ListType {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension ListType: Parameter {}


/*
enum ListType {
    
    case roadmap
    case backlog
    case okr
    case development
    case wins
    
}
*/


