//
//  Share.swift
//  App
//
//  Created by Richard Beck on 2019-03-04.
//

import Foundation
import FluentMySQL
import Vapor


final class Share: MySQLModel {
      typealias Database = MySQLDatabase
    
    var id: Int?
    var objectType: String
    var objectId: String?
    var accessKey: String?
    var expiration: Date?
    var createdAt: Date?
    var updatedAt: Date?
    
    
    init(id: Int?, objectType:String, objectId:String?, accessKey: String?, expiration:Date?) {
        self.id = id
        self.objectType = objectType
        self.objectId = objectId
        self.accessKey = accessKey
        self.expiration = expiration
        
        
    }
    
    
}


extension Share: Content {}
extension Share: Migration {}
extension Share {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension Share: Parameter {}
