//
//  ChangePassword.swift
//  App
//
//  Created by Richard Beck on 2019-03-02.
//

import Foundation
import FluentMySQL
import Vapor


final class ChangePassword: MySQLModel {
    
      typealias Database = MySQLDatabase
    
    var id: Int?
    var userId: Int
    var accessKey: String
    var createdAt: Date?
    var updatedAt: Date?
    
    
    init(id: Int?, userId:Int, accessKey: String) {
        self.id = id
        self.userId = userId
        self.accessKey = accessKey
        
        
        
    }
    
    
}


extension ChangePassword: Content {}
extension ChangePassword: Migration {}
extension ChangePassword {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension ChangePassword: Parameter {}
