//
//  User.swift
//  App
//
//  Created by Richard on 2019-02-01.
//

import Foundation
import FluentMySQL
import Vapor
import Authentication


final class User: MySQLModel {
      typealias Database = MySQLDatabase
    
    var id: Int?
    var email: String  // added
    var password: String  // added
    var createdAt: Date?
    var updatedAt: Date?
    
    init(id: Int? = nil, email: String, password: String) {
        self.id = id
        self.email = email
        self.password = password
        
    }
    
    var tokens: Children<User, UserToken> {
        return children(\.userID)
    }
    
}

extension User: PasswordAuthenticatable {
    static var usernameKey: WritableKeyPath<User, String> {
        return \User.email
    }
    static var passwordKey: WritableKeyPath<User, String> {
        return \User.password
    }
}


extension User {
    struct Public: Content {
        let id: Int
        let email: String
        
    }
}

extension User: Content {}
extension User: Migration {}
extension User: SessionAuthenticatable { }
extension User {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension User: Parameter {}

final class UserToken: MySQLModel {
    
    var id: Int?
    var string: String
    var userID: User.ID
    
    var user: Parent<UserToken, User> {
        return parent(\.userID)
    }
}

extension UserToken: Token {
    /// See `Token`.
    typealias UserType = User
    
    /// See `Token`.
    static var tokenKey: WritableKeyPath<UserToken, String> {
        return \.string
    }
    
    /// See `Token`.
    static var userIDKey: WritableKeyPath<UserToken, User.ID> {
        return \.userID
    }
}

extension User: TokenAuthenticatable {
    /// See `TokenAuthenticatable`.
    typealias TokenType = UserToken
}


extension User: Validatable {
    /// See `Validatable`.
    static func validations() throws -> Validations<User> {
        var validations = Validations(User.self)
        // define validations
        try validations.add(\.email, .email)
        return validations
    }
}
