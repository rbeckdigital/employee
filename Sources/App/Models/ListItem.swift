//
//  ListItem.swift
//  App
//
//  Created by Richard Beck on 2019-02-19.
//

import Foundation
import FluentMySQL
import Vapor


final class ListItem: MySQLModel {
      typealias Database = MySQLDatabase
    
    var id: Int?
    var userId: Int
    var type: String
    var title: String
    var description: String?
    var dueDate: Date?
    var order:Int
    var employeeId: Int
    var externalReference: String?
    var status: String
    var labels: String?
    var createdAt: Date?
    var updatedAt: Date?
    
    
    init(id: Int?, userId:Int, type: String, title:String, description:String?, dueDate:Date?, order:Int, employeeId:Int, externalReference:String?, status:String, labels:String?) {
        self.id = id
        self.userId = userId
        self.type = type
        self.title = title
        self.description = description
        self.dueDate = dueDate
        self.order = order
        self.employeeId = employeeId
        self.externalReference = externalReference
        self.status = status
        self.labels = labels
    }
    
    
}


extension ListItem: Content {}
extension ListItem: Migration {}
extension ListItem {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension ListItem: Parameter {}


extension User {
    // this galaxy's related planets
    var listItem: Children<User, ListItem> {
        return children(\.userId)
    }
}

extension ListItem {
    // this planet's related galaxy
    var user: Parent<ListItem, User> {
        return parent(\.userId)
    }
}




struct ShowItem:Encodable {
    
    var id: Int?
    var userId: Int
    var type: Future<String>
    var title: String
    var description: String?
    var dueDate: Date?
    var order:Int
    var employeeName: Future<String>
    var externalReference: String?
    var externalReferenceType: Bool
    var status: String
    var labels: [String]?
    var createdAt: Date?
    var updatedAt: Date?
    
}
