//
//  Employee+User.swift
//  App
//
//  Created by Richard Beck on 2019-02-23.
//

import Foundation
import FluentMySQL
import Vapor


final class EmployeeUser: MySQLModel {
      typealias Database = MySQLDatabase
    
    var id:Int?
    var employeeId: Int
    var userId: Int
    var createdAt: Date?
    var updatedAt: Date?
    
    
    init(id:Int?, employeeId: Int, userId:Int) {
        self.employeeId = employeeId
        self.userId = userId
 
    }
    
}


extension EmployeeUser: Content {}
extension EmployeeUser: Migration {}
extension EmployeeUser {
    static var createdAtKey: TimestampKey? = \.createdAt
    static var updatedAtKey: TimestampKey? = \.updatedAt
}
extension EmployeeUser: Parameter {}
