//
//  ChangePasswordController.swift
//  App
//
//  Created by Richard Beck on 2019-03-02.
//

import Foundation
import Vapor
import Crypto
import Fluent
import FluentMySQL
import Mailgun
import SendGrid



final class ChangePasswordController {

    struct cpResponse:Content {
        var success: Bool
        var message:String?
    }
    
    struct emailConfig {
        var to: [String:String]
    }
    
func setChange(_ req: Request) throws -> Future<cpResponse> {
    let email = try req.parameters.next(String.self)
    return User.query(on: req)
        .filter(\.email == email)
        .first()
        .map {user in
            
            let success = true
            let message:String? = nil
            
            if user == nil {throw Abort(.badRequest, reason:"email address was not found. Please check and try again.")}
            
            let cpRequest = ChangePassword(id: nil, userId: user!.id!, accessKey:UUID().uuidString)
            _ = cpRequest.save(on:req)
            
            
            // SendGrid
            let toEmail = EmailAddress(email: email, name: "Richard")
            let fromEmail = EmailAddress(email:"noreply@team.digital-possibilities.ca", name: "Team Activities")
            let personalization = Personalization(to: [toEmail], cc: nil, bcc: nil, subject: "Your Request to Change Password", headers: nil, substitutions: nil, dynamicTemplateData: nil, customArgs: nil, sendAt: nil)
            let SGemail = SendGridEmail(personalizations: [personalization], from: fromEmail, replyTo: nil, subject: "Your Request to Change Password", content: [["type": "text/plain",  "value": "Click on the following URL to reset your password: https://team.digital-possibilities.ca/changepassword/\(cpRequest.accessKey)."]], attachments: nil, templateId: nil, sections: nil, headers: nil, categories: nil, customArgs: nil, sendAt: nil, batchId: nil, asm: nil, ipPoolName: nil, mailSettings: nil, trackingSettings: nil)
            
            let sendGridClient = try req.make(SendGridClient.self)
            
            _ = try sendGridClient.send([SGemail], on: req.eventLoop)
            
            return cpResponse(success:success,message:message)
    }
    
    
    }

    func renderPasswordChangeUI(_ req: Request) throws -> Future<View> {
         let accesskey = try req.parameters.next(String.self)
         let limitDate = Date().addingTimeInterval(-1209600)
        return ChangePassword.query(on: req)
            .filter(\.accessKey == accesskey)
            .filter(\.createdAt > limitDate )
            .first()
            .flatMap { cpRequest in
                if cpRequest == nil {throw Abort(.notFound, reason:"No request found.")}
                return User.find(cpRequest!.userId, on:req).flatMap {user in
                    if user == nil {throw Abort(.notFound, reason:"No user found.")}
                    return try req.view().render("passwordChange", ["accessKey":accesskey, "email":user!.email])
                }
                
        }
    }
    
    struct PasswordChangeRequest:Decodable {
        var password: String
        var accessKey: String
    }
    
    func requestPasswordChange(_ req: Request) throws -> Future<View> {
          let limitDate = Date().addingTimeInterval(-1209600)
          return try req.content.decode(PasswordChangeRequest.self).flatMap { passwordChangeRequest in
            
            if passwordChangeRequest.password.count < 10 {
                throw Abort(.badRequest, reason:"A password must have at least 10 characters.")
                
            }
            
            return ChangePassword.query(on: req)
                    .filter(\.accessKey == passwordChangeRequest.accessKey)
                    .filter(\.createdAt > limitDate )
                    .first()
                    .flatMap { cpRequest in
                        if cpRequest == nil {throw Abort(.notFound, reason:"No user found.")}
                       
                        _ = try self.updatePassword(req, userId: (cpRequest?.userId)!, password: passwordChangeRequest.password)
                        
                        return try req.view().render("passwordChange", ["status":"Changed"])
        }
      }
    }
    
    
    
    func updatePassword(_ req: Request, userId:Int, password:String) throws -> Future<User> {
        return User.find(userId, on: req).flatMap { user in
            if user == nil {throw Abort(.notFound)}
            
            let hasher = try req.make(BCryptDigest.self)
            let passwordHashed = try hasher.hash(password)
                user?.password = passwordHashed

                
                return (user?.save(on: req))!
            }
        }
    
    func renderPCR(_ req: Request) throws -> Future<View> {
        var email:String = ""
        if let qEmail = try? req.query.get(String.self, at: "email") {
            email = qEmail
        }
        
        return try req.view().render("passwordChangeRequest", ["email":email])
    }
}
