//
//  EmployeeController.swift
//  App
//
//  Created by Richard Beck on 2019-02-19.
//

import Foundation
import Vapor
import Fluent


final class EmployeeController {
    
    let imageFolder = "/images/"
    
    func list(_ req: Request) throws -> Future<[Employee]> {
        return Employee.query(on: req).all()
    }
    
    func employee(_ req: Request) throws -> Future<Employee> {
        return Employee.find(try req.parameters.next(Int.self), on:req).map {employee in
            if employee == nil {throw Abort(.notFound)}
            return employee!
        }
    }
    
    func create(_ req: Request) throws -> Future<Employee> {
        let userId = Int(try req.session()["user"] ?? "0")
        return try flatMap(to: Employee.self, req.content.decode( Employee.self), req.content.decode( ImageUploadData.self)) { employee, imageData in
            
            if imageData.picture.count > 50 {
                let workPath = try req.make(DirectoryConfig.self).workDir
                let photo = "\(employee.name)-\(UUID().uuidString).jpg"
                let path = workPath + "Public" + self.imageFolder + photo
                FileManager().createFile(atPath: path, contents: imageData.picture, attributes: nil)
                employee.photo = self.imageFolder + photo
            }
            
            let newEmployee = employee.save(on: req)
            _ = newEmployee.map {employee -> EventLoopFuture<EmployeeUser> in let employeeU = EmployeeUser(id: nil, employeeId: employee.id!, userId: userId!)
                                                return employeeU.save(on: req)
            }
            
            return newEmployee
        }
    }
    
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(Employee.self).flatMap { employee in
            return employee.delete(on: req)
            }.transform(to: .ok)
    }
    
    func update(_ req: Request) throws -> Future<Employee> {
        return Employee.find(try req.parameters.next(Int.self), on: req).flatMap { employee in
            if employee == nil {throw Abort(.notFound)}
            return try req.content.decode(Employee.self).flatMap { newEmployee in
                employee?.name = newEmployee.name
                employee?.photo = newEmployee.photo
                employee?.background = newEmployee.background
                employee?.email = newEmployee.email

                return (employee?.save(on: req))!
            }
        }
    }
    
    func updateII(_ req: Request) throws -> Future<Employee> {
            let employeeId = try req.parameters.next(Int.self)
            return try flatMap(to: Employee.self, req.content.decode( Employee.self), req.content.decode( ImageUploadData.self)) { employee, imageData in
                
                Employee.find(employeeId, on: req).flatMap { oldemployee in
                        if oldemployee == nil {throw Abort(.notFound)}
                        oldemployee?.name = employee.name
                        oldemployee?.background = employee.background
                        oldemployee?.email = employee.email
                    
        
                if imageData.picture.count > 50 {
                    let workPath = try req.make(DirectoryConfig.self).workDir
                    let photo = "\(employee.name)-\(UUID().uuidString).jpg"
                    let path = workPath + "Public" + self.imageFolder + photo
                    FileManager().createFile(atPath: path, contents: imageData.picture, attributes: nil)
                    if oldemployee?.photo != nil {
                            try FileManager().removeItem(atPath: workPath+"Public"+(oldemployee?.photo)!) }
                    oldemployee?.photo = self.imageFolder + photo
                }
                return (oldemployee?.save(on: req))!
        
                }
            }
    }
    
}

struct ImageUploadData: Content {
    var picture: Data
}
