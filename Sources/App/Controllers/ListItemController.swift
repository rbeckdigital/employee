//
//  ListItemController.swift
//  App
//
//  Created by Richard Beck on 2019-02-19.
//

import Foundation
import Vapor
import Fluent
import FluentMySQL
import FluentSQL


final class ListItemController {
    
    func list(_ req: Request) throws -> Future<[ListItem]> {
        return ListItem.query(on: req).all()
    }
    
    
    func filteredlist(_ req: Request) throws -> Future<[ListItem]> {
            let listType = try req.parameters.next(String.self)
            let employee = try req.parameters.next(Int.self)
            guard let userId = Int(try req.session()["user"] ?? "0") else { throw Abort(.badRequest) }
        return ListItem.query(on: req)
            .filter(\.type == listType)
            .filter(\.employeeId == employee)
            .filter(\.userId == userId)
            .sort(\.order, .ascending).all()
    }
    
    
    func create(_ req: Request) throws -> Future<ListItem> {
        return try req.content.decode(ListItem.self).flatMap { listItem in
            if try req.session()["user"] == nil {throw Abort(.notFound)} else
                {listItem.userId = Int(try req.session()["user"]!)!}
            return listItem.save(on: req)
        }
    }
    
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(ListItem.self).flatMap { listItem in
            return listItem.delete(on: req)
            }.transform(to: .ok)
    }
    
    func update(_ req: Request) throws -> Future<ListItem> {
        return ListItem.find(try req.parameters.next(Int.self), on: req).flatMap { listItem in
            if listItem == nil {throw Abort(.notFound)}
            return try req.content.decode(ListItem.self).flatMap { newListItem in
                
                listItem?.userId = newListItem.userId
                listItem?.type = newListItem.type
                listItem?.title = newListItem.title
                listItem?.description = newListItem.description
                listItem?.dueDate = newListItem.dueDate
                listItem?.order = newListItem.order
                listItem?.employeeId = newListItem.employeeId
                listItem?.externalReference = newListItem.externalReference
                listItem?.labels = newListItem.labels
                listItem?.status = newListItem.status

                return (listItem?.save(on: req))!
            }
        }
    }
    
    
    func updateOrder(_ req: Request) throws -> Future<HTTPStatus> {
 
        let listType = try req.parameters.next(String.self)
        let employee = try req.parameters.next(Int.self)

        return ListItem.query(on: req).filter(\.type == listType).filter(\.employeeId == employee).filter(\.status == "open").sort(\.order, .ascending).all().flatMap { itemList in
            return try req.content.decode( ListOrder.self).map { newOrder in
              
                print(newOrder)
                print(newOrder)

                itemList.map { item in
                
                    print(item)
                    print(item)

                    let newPostion=newOrder.neworder.firstIndex(of: item.id!) ?? 0 + 1

                   _ = ListItem.find(item.id!, on:req).map { listItem -> EventLoopFuture<ListItem> in

                                listItem?.order = newPostion

                        return (listItem?.save(on: req))!
                    }
                }
                    
                
            }.transform(to: .ok)
        }
    }
    
    
}


