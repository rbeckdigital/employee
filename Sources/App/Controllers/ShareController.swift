//
//  ShareController.swift
//  App
//
//  Created by Richard Beck on 2019-03-04.
//

import Foundation
import Vapor
import Fluent


final class ShareController {
    
    func list(_ req: Request) throws -> Future<[Share]> {
        return Share.query(on: req).all()
    }
    
    func share(_ req: Request) throws -> Future<Share> {
        return Share.find(try req.parameters.next(Int.self), on:req).map {share in
            if share == nil {throw Abort(.notFound)}
            return share!
        }
    }
    
    func create(_ req: Request) throws -> Future<Share> {
        return try req.content.decode(Share.self).flatMap { share in
            share.id = nil
            share.accessKey = UUID().uuidString
            return share.save(on: req)
        }
    }
    

    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(Share.self).flatMap { share in
            return share.delete(on: req)
            }.transform(to: .ok)
    }
    
    func deleteById(_ req: Request, shareId:Int) throws -> Future<HTTPStatus> {
        return Share.find(shareId, on:req).flatMap { share in
            return (share?.delete(on: req))!
            }.transform(to: .ok)
    }
    
    func update(_ req: Request) throws -> Future<Share> {
        return Share.find(try req.parameters.next(Int.self), on: req).flatMap { share in
            if share == nil {throw Abort(.notFound)}
            return try req.content.decode(Share.self).flatMap { newShare in
                share?.accessKey = newShare.accessKey
                share?.objectId = newShare.objectId
                share?.objectType = newShare.objectType
                
                return (share?.save(on: req))!
            }
        }
    }
    
    
}
