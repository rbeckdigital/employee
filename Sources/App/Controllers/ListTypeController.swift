//
//  ListType.swift
//  App
//
//  Created by Richard Beck on 2019-02-28.
//

import Foundation
import Vapor
import Fluent


final class ListTypeController {
    
    func list(_ req: Request) throws -> Future<[ListType]> {
        return ListType.query(on: req).all()
    }
    
    func listType(_ req: Request) throws -> Future<ListType> {
        return ListType.find(try req.parameters.next(Int.self), on:req).map {listType in
            if listType == nil {throw Abort(.notFound)}
            return listType!
        }
    }
    
    func create(_ req: Request) throws -> Future<ListType> {
        let userId = Int(try req.session()["user"] ?? "0")
        return try req.content.decode(ListType.self).flatMap { listType in
            listType.userId = userId!
            listType.id = nil
            return listType.save(on: req)
        }
    }
    
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(ListType.self).flatMap { listType in
            return listType.delete(on: req)
            }.transform(to: .ok)
    }
    
    func update(_ req: Request) throws -> Future<ListType> {
        return ListType.find(try req.parameters.next(Int.self), on: req).flatMap { listType in
            if listType == nil {throw Abort(.notFound)}
            return try req.content.decode(ListType.self).flatMap { newListType in
                listType?.name = newListType.name
                listType?.typeCode = newListType.typeCode
                listType?.userId = newListType.userId
                listType?.column = newListType.column
                listType?.order = newListType.order
                
                return (listType?.save(on: req))!
            }
        }
    }
    
    
}
