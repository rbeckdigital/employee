//
//  ListColumnController.swift
//  App
//
//  Created by Richard Beck on 2019-02-28.
//

import Foundation
import Vapor
import Fluent


final class ListColumnController {
    
    func list(_ req: Request) throws -> Future<[ListColumn]> {
        return ListColumn.query(on: req).all()
    }
    
    func listColumn(_ req: Request) throws -> Future<ListColumn> {
        return ListColumn.find(try req.parameters.next(Int.self), on:req).map {listColumn in
            if listColumn == nil {throw Abort(.notFound)}
            return listColumn!
        }
    }
    
    func create(_ req: Request) throws -> Future<ListColumn> {
        let userId = Int(try req.session()["user"] ?? "0")
        return try req.content.decode(ListColumn.self).flatMap { listColumn in
            listColumn.userId = userId!
            listColumn.id = nil
            return listColumn.save(on: req)
        }
    }
    
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(ListColumn.self).flatMap { listColumn in
            return listColumn.delete(on: req)
            }.transform(to: .ok)
    }
    
    func update(_ req: Request) throws -> Future<ListColumn> {
        return ListColumn.find(try req.parameters.next(Int.self), on: req).flatMap { listColumn in
            if listColumn == nil {throw Abort(.notFound)}
            return try req.content.decode(ListColumn.self).flatMap { newListColumn in
                listColumn?.column = newListColumn.column
                listColumn?.userId = newListColumn.userId
                listColumn?.name = newListColumn.name
                listColumn?.visibility = newListColumn.visibility

                return (listColumn?.save(on: req))!
            }
        }
    }
    
    
}
