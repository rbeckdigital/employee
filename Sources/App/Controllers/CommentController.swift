//
//  CommentController.swift
//  App
//
//  Created by Richard Beck on 2019-02-21.
//

import Foundation
import Vapor
import Fluent
import FluentMySQL
import FluentSQL

final class CommentController {
    
    func list(_ req: Request) throws -> Future<[Commenting]> {
        return Commenting.query(on: req).all()
    }
    
    func Comment(_ req: Request) throws -> Future<Commenting> {
        return Commenting.find(try req.parameters.next(Int.self), on:req).map {commenting in
            if commenting == nil {throw Abort(.notFound)}
            return commenting!
        }
    }
    
    func create(_ req: Request) throws -> Future<Commenting> {
        let userId = Int(try req.session()["user"] ?? "0")
        return try req.content.decode(Commenting.self).flatMap { commenting in
            commenting.userId = userId!
            return commenting.save(on: req)
        }
    }
    
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(Commenting.self).flatMap { commenting in
            return commenting.delete(on: req)
            }.transform(to: .ok)
    }
    
    func update(_ req: Request) throws -> Future<Commenting> {
        return Commenting.find(try req.parameters.next(Int.self), on: req).flatMap { commenting in
            if commenting == nil {throw Abort(.notFound)}
            return try req.content.decode(Commenting.self).flatMap { newCommenting in
                commenting?.comment = newCommenting.comment
                
                
                return (commenting?.save(on: req))!
            }
        }
    }


}
