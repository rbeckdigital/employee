//
//  EmployeeUser.swift
//  App
//
//  Created by Richard Beck on 2019-03-01.
//

import Foundation
import Vapor
import Fluent


final class EmployeeUserController {
    
    func list(_ req: Request) throws -> Future<[EmployeeUser]> {
        return EmployeeUser.query(on: req).all()
    }
    
    func employee(_ req: Request) throws -> Future<EmployeeUser> {
        return EmployeeUser.find(try req.parameters.next(Int.self), on:req).map {object in
            if object == nil {throw Abort(.notFound)}
            return object!
        }
    }
    
    func create(_ req: Request) throws -> Future<EmployeeUser> {
        return try req.content.decode(EmployeeUser.self).flatMap { object in
            return object.save(on: req)
        }
    }
    
    
    func delete(_ req: Request) throws -> Future<HTTPStatus> {
        return try req.parameters.next(EmployeeUser.self).flatMap { object in
            return object.delete(on: req)
            }.transform(to: .ok)
    }
    
    func update(_ req: Request) throws -> Future<EmployeeUser> {
        return EmployeeUser.find(try req.parameters.next(Int.self), on: req).flatMap { object in
            if object == nil {throw Abort(.notFound)}
            return try req.content.decode(EmployeeUser.self).flatMap { newObject in
                object?.userId = newObject.userId
                object?.employeeId = newObject.employeeId
                return (object?.save(on: req))!
            }
        }
    }
    
    
}
