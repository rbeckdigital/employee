//
//  UserController.swift
//  App
//
//  Created by Richard on 2019-02-01.
//

import Foundation
import Vapor
import Crypto
import Fluent
import FluentMySQL

final class UserController {
    
    func setUp(_ req: Request) throws -> Future<Future<User.Public>> {
        
        return User.query(on: req).first().map() {
            user -> EventLoopFuture<User.Public> in

            
            if  user != nil {
                throw Abort(.unauthorized)
            } else {
                return try self.register(req)
            }
            
        }

    }
    
    
    func register(_ req: Request) throws -> Future<User.Public> {
        return try req.content.decode(User.self).flatMap { user in
            let hasher = try req.make(BCryptDigest.self)
            let passwordHashed = try hasher.hash(user.password)
            let newUser = User(email: user.email, password: passwordHashed)
            return newUser.save(on: req).map { storedUser in
                return User.Public(
                    id: try storedUser.requireID(),
                    email: storedUser.email
                )
            }
        }
    }
    

    
    
    struct Invite: Content {
        var code: String

    
    }
    
    func registerInvite(_ req: Request) throws -> Future<User.Public> {
        return flatMap(to: User.Public.self,  try req.content.decode(User.self), try req.content.decode(Invite.self)) { user, invite in
            return Share.query(on: req)
                .filter(\Share.accessKey == invite.code)
                .filter(\.objectType == "register")
                .first()
                .flatMap {key -> Future<User.Public> in
                    
                    if key == nil {
                        print("key: "+(key?.accessKey ?? "Not Found"))
                        throw Abort(.badRequest, reason: "Invite Code is not Valid")}
                    let publicUser = try self.registerUser(req, user:user, shareId:key!.id!)
                    return publicUser
                }
            
            
            
            
            }
        }


    func registerUser(_ req: Request, user:User, shareId:Int) throws -> Future<User.Public> {
    return User.query(on:req)
                .filter(\User.email == user.email)
                .first()
                .flatMap { founduser  in
                    if founduser != nil {  throw Abort(.badRequest, reason: "Email is already taken") }
                            let hasher = try req.make(BCryptDigest.self)
                            let passwordHashed = try hasher.hash(user.password)
                            if user.password.count < 10 {  throw Abort(.badRequest, reason: "Password must be at least 10 characters long.") }
                            let newUser = User(email: user.email, password: passwordHashed)
                            try newUser.validate()
                            return newUser.save(on: req).map { storedUser in
                                _ = try ShareController().deleteById(req, shareId:shareId)
                                let ud = UserDetails(id: nil, userId: storedUser.id!, name: nil)
                                _ = ud.save(on: req)
                                return User.Public(
                                    id: try storedUser.requireID(),
                                    email: storedUser.email
                                    )
                            
                                }
        }
    
    }


    
    func login(_ req: Request) throws -> Future<User.Public> {
        return try req.content.decode(User.self).flatMap { user in
            return User.authenticate(
                username: user.email,
                password: user.password,
                using: BCryptDigest(),
                on: req
                ).map { user in
                    guard let user = user else {
                        throw Abort(.unauthorized)
                    }
                    try req.authenticateSession(user)
                    try req.session()["logged"] = "true"
                    try req.session()["user"] = String(user.id!)
                    return User.Public(id:try user.requireID(), email:user.email)
            }
        }
    }
    
    
    
    func tokenLogin(_ req: Request) throws -> Future<Int> {
        return try req.content.decode(User.self).flatMap { user in
            return User.authenticate(
                username: user.email,
                password: user.password,
                using: BCryptDigest(),
                on: req
                ).map { user in
                    guard let user = user else {
                        throw Abort(.unauthorized)
                    }
                    try req.authenticateSession(user)
                    try req.session()["logged"] = "true"
                    print(UserToken.self)
                    return UserToken.tokenKey.hashValue
            }
        }
    }
    
    
    
    
    func profile(_ req: Request) throws -> String {
        let user = try req.requireAuthenticated(User.self)
        return "You're viewing \(user.email) profile."
    }
    
    func logout(_ req: Request) throws -> Future<String> {
        try req.unauthenticateSession(User.self)
        return Future.map(on: req) { return "true" }
    }
    
    func authStatus(_ req: Request) throws -> Future<String> {
        return req.future("{\"status\":\(try req.isAuthenticated(User.self))}")
    }
    
    func user(_ req:Request, userId:Int) throws -> Future<User?> {
        return User.find(userId, on:req)
    }

    struct UserEmail: Content {
        var id:Int
        var email:String
    }
    
    func useruserDetails(_ req: Request) throws -> Future<UserEmail> {
        let userId = Int(try req.session()["user"] ?? "0")
        return try flatMap(to: UserEmail.self, req.content.decode( UserEmail.self), req.content.decode( UserDetails.self)) { user, userDetails in
            
           _ = User.find(userId!, on: req).map { olduser -> Future<User> in
                olduser?.email = user.email
                return olduser!.save(on: req)
            }
            
            _ = UserDetails.query(on:req)
                .filter(\UserDetails.userId == userId!)
                .first().flatMap { olduserdetails -> EventLoopFuture<UserDetails> in
                    if olduserdetails == nil {
                        let ud = UserDetails(id: nil, userId: userId!, name: userDetails.name)
                        return ud.save(on: req)
                        
                    }
                    
                olduserdetails?.name = userDetails.name
                    return olduserdetails?.save(on: req) ?? req.future(userDetails)
                    
            }
            
            return req.future(user)
    }
    
    }
    
    
    
    
}
