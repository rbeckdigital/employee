import Vapor
import Authentication

/// Register your application's routes here.
public func routes(_ router: Router) throws {
   
    
    // Security //
    let userController = UserController()
    router.post("setup", use: userController.setUp)
    
    
    let authSessionRouter = router.grouped(User.authSessionsMiddleware())
    let auth = authSessionRouter.grouped(RedirectMiddleware<User>(path: "/login"))
    authSessionRouter.post("login", use: userController.login)
    auth.get("logout", use: userController.logout)
    authSessionRouter.post("tokenLogin", use: userController.tokenLogin)
    auth.get("profile", use: userController.profile)
    authSessionRouter.get("authStatus", use: userController.authStatus)
    authSessionRouter.post("register","invite", use: userController.registerInvite)
    
    
    // Employee //
    let employeeController = EmployeeController()
    auth.get("employee", Int.parameter,use: employeeController.employee)
    auth.post("employee", use: employeeController.create)
    auth.post("employee", Int.parameter, use: employeeController.updateII)
    auth.delete("employee", Employee.parameter, use: employeeController.delete)

    // ListItem //
    let listItemController = ListItemController()
    auth.get("item",String.parameter ,Int.parameter,use: listItemController.filteredlist)
    auth.post("item", use: listItemController.create)
    auth.patch("item", Int.parameter, use: listItemController.update)
    auth.delete("item", ListItem.parameter, use: listItemController.delete)
    auth.post("item","updateOrder",String.parameter ,Int.parameter,use: listItemController.updateOrder)
    
    // Comments //
    let commentController = CommentController()
    auth.get("comment", Int.parameter,use: commentController.Comment)
    auth.post("comment", use: commentController.create)
    auth.patch("comment", Int.parameter, use: commentController.update)
    auth.delete("comment", Commenting.parameter, use: commentController.delete)
    
    // ListType //
    let listTypeController = ListTypeController()
    auth.get("listtype", Int.parameter,use: listTypeController.listType)
    auth.post("listtype", use: listTypeController.create)
    auth.patch("listtype", Int.parameter, use: listTypeController.update)
    auth.delete("listtype", ListType.parameter, use: listTypeController.delete)
    
    // ListColumn//
    let listColumnController = ListColumnController()
    auth.get("listcolumn", Int.parameter,use: listColumnController.listColumn)
    auth.post("listcolumn", use: listColumnController.create)
    auth.patch("listcolumn", Int.parameter, use: listColumnController.update)
    auth.delete("listcolumn", ListColumn.parameter, use: listColumnController.delete)
    
    
    // Share //
    let shareController = ShareController()
    auth.get("share", Int.parameter,use: shareController.share)
    auth.post("share", use: shareController.create)
    auth.patch("share", Int.parameter, use: shareController.update)
    auth.delete("share", Share.parameter, use: commentController.delete)
    
    
    // UI //
    let uiController = UIController()
    router.get("login", use: uiController.renderLogin)
    router.get("register", use: uiController.renderRegister)
    auth.get("item","info",Int.parameter, use: uiController.renderItemInfo)
    auth.get("item","readonly",Int.parameter, use: uiController.renderItemReadOnly)
    auth.get("item","new",String.parameter,"employee",Int.parameter, use: uiController.renderItemNew)
    auth.get("comment","list",Int.parameter, use: uiController.renderComments)
    auth.get("employee","list", use: uiController.renderEmployees)
    auth.get("", use: uiController.renderHome)
    auth.get("overview", use: uiController.renderOverview)
    auth.get("comment","edit",Int.parameter, use: uiController.renderEditComment)
    auth.get("tasklist", use: uiController.renderTaskList)
    
    
    // UI Components //
    auth.get("component","item",String.parameter,Int.parameter, use: uiController.renderItems)
    
    
    // UI Employee//
    let uiControllerEmployee = UIControllerEmployee()
    auth.get("employee",Int.parameter, use: uiControllerEmployee.renderEmployee)
  

    // UI ColumnAdmin//
    let uiControllerColumnAdmin = UIControllerColumnAdmin()
    auth.get("column","edit", use: uiControllerColumnAdmin.renderColumnAdministration)
    auth.get("column","mutate",Int.parameter, use: uiControllerColumnAdmin.renderEditColumn)
    auth.get("column","new", use: uiControllerColumnAdmin.renderNewColumn)
    auth.get("list",Int.parameter,"edit", use: uiControllerColumnAdmin.renderListAdministration)
    auth.get("list","edit", use: uiControllerColumnAdmin.renderListAdministration)
    auth.get("list","mutate",Int.parameter, use: uiControllerColumnAdmin.renderEditList)
    auth.get("list","new","column",Int.parameter, use: uiControllerColumnAdmin.renderNewList)
    
    
    // UI ColumnAdmin//
    let uiControllerEmployeeManagement = UIControllerEmployeeManagement()
    auth.get("employee","management","list", use: uiControllerEmployeeManagement.renderEmployees)
    auth.get("employee","management","edit", Int.parameter, use: uiControllerEmployeeManagement.renderEditEmployee)
    auth.get("employee","management","new", use: uiControllerEmployeeManagement.renderNewEmployee)
    
    // UI UserAdmin//
    let uiControllerAccount = UIControllerAccount()
    auth.get("user","management", use: uiControllerAccount.renderAccount)
    auth.post("user","details", use: userController.useruserDetails)
   
    
    // UI Changepassword//
    let changePasswordController = ChangePasswordController()
    router.get("password","change", String.parameter,use: changePasswordController.setChange)
    router.get("changepassword", String.parameter,use: changePasswordController.renderPasswordChangeUI)
    router.post("changepassword", use: changePasswordController.requestPasswordChange)
    router.get("password","change","request",use: changePasswordController.renderPCR)
}



