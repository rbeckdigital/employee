
var globallistItemId = 0;

function getItemList(type,employee,location) {


$.ajax
({

url: '/component/item/'+type+'/'+employee,
type: 'get',
processData: false,
contentType: 'application/json',
success: function(result){document.getElementById(location).innerHTML = result; },
error: function(XMLHttpRequest, textStatus, errorThrown){document.getElementById('error').innerHTML = '<div class="bs-callout bs-callout-danger">Error: '+ textStatus + ' '+errorThrown+'</div>';}

});


var fixHelperModified = function(e, tr) {
var $originals = tr.children();
var $helper = tr.clone();
$helper.children().each(function(index) {
$(this).width($originals.eq(index).width())
});
return $helper;
};

$("#table_"+location+" tbody").sortable({
                                   helper: fixHelperModified,
                                   axis: 'y',
                                   update: function (e, ui) {
                                   
                                   var list = new Array();
                                   $("#table_"+location).find('.index').each(function(){
                                                                                 var id=$(this).attr('data-id');
                                                                                 list.push(Number(id));
                                                                           
                                                                                 });
                                   
                                  
                                   var postData = JSON.stringify({neworder:list});

                                   
                                   let url = '/item/updateOrder/'+type+'/'+employee;
                                   
                                   
                                   $.ajax
                                   ({
                                    
                                    url: url,
                                    type: 'post',
                                    processData: false,
                                    contentType: 'application/json',
                                    data:postData,
                                    error: function(XMLHttpRequest, textStatus, errorThrown){document.getElementById('error').innerHTML = '<div class="bs-callout bs-callout-danger">Error: '+ textStatus + ' '+errorThrown+'</div>';}
                                    
                                    });
                                   

                                   }
}).disableSelection();




}


function getComments(listItemId) {
    
    $.ajax
    ({
     
     url: '/comment/list/'+listItemId,
     type: 'get',
     processData: false,
     contentType: 'application/json',
     success: function(result){document.getElementById('comments').innerHTML = result; },
     error: function(XMLHttpRequest, textStatus, errorThrown){document.getElementById('error').innerHTML = '<div class="bs-callout bs-callout-danger">Error: '+ textStatus + ' '+errorThrown+'</div>';}
     
     });
    
}

function getUpdateItem(listItemId) {
    
    $.ajax
    ({
     
     url: '/item/readonly/'+listItemId,
     type: 'get',
     processData: false,
     contentType: 'application/json',
     success: function(result){document.getElementById('itemUpdate').innerHTML = result; },
     error: function(XMLHttpRequest, textStatus, errorThrown){document.getElementById('error').innerHTML = '<div class="bs-callout bs-callout-danger">Error: '+ textStatus + ' '+errorThrown+'</div>';}
     
     });
    
}

function getUpdateItemEdit(listItemId) {
    
    $.ajax
    ({
     
     url: '/item/info/'+listItemId,
     type: 'get',
     processData: false,
     contentType: 'application/json',
     success: function(result){document.getElementById('itemUpdate').innerHTML = result; },
     error: function(XMLHttpRequest, textStatus, errorThrown){document.getElementById('error').innerHTML = '<div class="bs-callout bs-callout-danger">Error: '+ textStatus + ' '+errorThrown+'</div>';}
     
     });
    
}


function getNewItem(itemType, employeeId) {
    
    $.ajax
    ({
     
     url: '/item/new/'+itemType+'/employee/'+employeeId,
     type: 'get',
     processData: false,
     contentType: 'application/json',
     success: function(result){document.getElementById('itemUpdate').innerHTML = result; },
     error: function(XMLHttpRequest, textStatus, errorThrown){document.getElementById('error').innerHTML = '<div class="bs-callout bs-callout-danger">Error: '+ textStatus + ' '+errorThrown+'</div>';}
     
     });
    
}


function selectItemOverlay(listItemId) {

    getUpdateItem(listItemId);
    getComments(listItemId);
    
    document.getElementById('AddCommentForm').hidden = false;
    
    globallistItemId = listItemId;

    UIkit.modal('#itemDetails').show();
    
}


function newItem(itemType, employeeId) {

    getNewItem(itemType, employeeId);
    
    document.getElementById('AddCommentForm').hidden = true;
    
    UIkit.modal('#itemDetails').show();
    
}

function editComment(commentId) {
    
    $.ajax
    ({
     
     url: '/comment/edit/'+commentId,
     type: 'get',
     processData: false,
     contentType: 'application/json',
     success: function(result){document.getElementById('updateComment').innerHTML = result; },
     error: function(XMLHttpRequest, textStatus, errorThrown){UIkit.notification({message: 'Error: '+errorThrown, pos: 'bottom-left'});}
     
     });
    
    
    UIkit.modal('#editItemOverlay').show();
    
}


function getEmployees(location) {
    
    $.ajax
    ({
     
     url: '/employee/list/',
     type: 'get',
     processData: false,
     contentType: 'application/json',
     success: function(result){document.getElementById(location).innerHTML = result; },
     error: function(XMLHttpRequest, textStatus, errorThrown){document.getElementById('error').innerHTML = '<div class="bs-callout bs-callout-danger">Error: '+ textStatus + ' '+errorThrown+'</div>';}
     
     });
    
}


function logout() {
    
    $.ajax
    ({
     url: '/logout',
     type: 'get',
     success: function(result){
     if(result == "true") {location.reload(true)};
     
     },
     
     error: function(XMLHttpRequest, textStatus, errorThrown){UIkit.notification({message: 'Error: '+errorThrown, pos: 'bottom-left'});}
     
     });
    
}




function mutateColumnOverlay(columnId) {
    
    getMutateColumn(columnId);

    UIkit.modal('#caMutate').show();
    
}


function getMutateColumn(columnId) {
    
    var url = "/column/new/";
    
    if (columnId) {url = "/column/mutate/"+columnId;}
    
    $.ajax
    ({
     
     url: url,
     type: 'get',
     processData: false,
     contentType: 'application/json',
     success: function(result){document.getElementById('updateColumn').innerHTML = result; },
     error: function(XMLHttpRequest, textStatus, errorThrown){UIkit.notification({message: 'Error: '+errorThrown, pos: 'bottom-left'});}
     
     });
    
}




function mutateListOverlay(listId,columnId) {
    
    getMutateList(listId,columnId);
    
    UIkit.modal('#listMutate').show();
    
}


function getMutateList(listId,columnId) {
    
    var url = "/list/new/column/"+columnId;
    
    if (listId) {url = "/list/mutate/"+listId;}
    
    $.ajax
    ({
     
     url: url,
     type: 'get',
     processData: false,
     contentType: 'application/json',
     success: function(result){document.getElementById('updateList').innerHTML = result; },
     error: function(XMLHttpRequest, textStatus, errorThrown){UIkit.notification({message: 'Error: '+errorThrown, pos: 'bottom-left'});}
     
     });
    
}


function mutateEmployeeOverlay(employeeId) {
    
    getEmployee(employeeId);
    
    UIkit.modal('#employeeEdit').show();
    
}


function getEmployee(employeeId) {
    
    var url = "/employee/management/new/";
    
    if (employeeId) {url = "/employee/management/edit/"+employeeId;}
    
    $.ajax
    ({
     
     url: url,
     type: 'get',
     processData: false,
     contentType: 'application/json',
     success: function(result){document.getElementById('employeeEditUpdate').innerHTML = result; },
     error: function(XMLHttpRequest, textStatus, errorThrown){UIkit.notification({message: 'Error: '+errorThrown, pos: 'bottom-left'});}
     
     });
    
}
