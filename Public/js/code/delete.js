



function deleteComment(objectId) {
    var result = confirm("Please confirm that you want to delete this comment");
    if (result) {
    
    $.ajax
    ({
     url: '/comment/'+objectId,
     type: 'delete',
     success: function(result){ UIkit.notification(' Deleted', {status: 'success', pos: 'bottom-left'});
     getComments(globallistItemId);
     },
     
     error: function(XMLHttpRequest, textStatus, errorThrown){UIkit.notification({message: 'Error: '+errorThrown, pos: 'bottom-left'});}
     
     });
    }
}




function deleteItem(objectId) {
    
    
    var result = confirm("Please confirm that you want to delete this item");
    if (result) {
    
    $.ajax
    ({
     url: '/item/'+objectId,
     type: 'delete',
     success: function(result){ UIkit.notification(' Deleted', {status: 'success', pos: 'bottom-left'});
     location.reload();
     },
     
     error: function(XMLHttpRequest, textStatus, errorThrown){UIkit.notification({message: 'Error: '+errorThrown, pos: 'bottom-left'});}
     
     });
    }
}


function deleteColumn(objectId) {
    
    
    var result = confirm("Please confirm that you want to delete this column");
    if (result) {
        
        $.ajax
        ({
         url: '/listcolumn/'+objectId,
         type: 'delete',
         success: function(result){ UIkit.notification(' Deleted', {status: 'success', pos: 'bottom-left'});
         location.reload();
         },
         
         error: function(XMLHttpRequest, textStatus, errorThrown){UIkit.notification({message: 'Error: '+errorThrown, pos: 'bottom-left'});}
         
         });
    }
}


function deleteList(objectId) {
    
    
    var result = confirm("Please confirm that you want to delete this list");
    if (result) {
        
        $.ajax
        ({
         url: '/listtype/'+objectId,
         type: 'delete',
         success: function(result){ UIkit.notification(' Deleted', {status: 'success', pos: 'bottom-left'});
         location.reload();
         },
         
         error: function(XMLHttpRequest, textStatus, errorThrown){UIkit.notification({message: 'Error: '+errorThrown, pos: 'bottom-left'});}
         
         });
    }
}


function deleteObject(type,objectId) {
    
    
    var result = confirm("Please confirm that you want to delete this "+type);
    if (result) {
        
        $.ajax
        ({
         url: '/'+type+'/'+objectId,
         type: 'delete',
         success: function(result){ UIkit.notification(' Deleted', {status: 'success', pos: 'bottom-left'});
         location.reload();
         },
         
         error: function(XMLHttpRequest, textStatus, errorThrown){UIkit.notification({message: 'Error: '+errorThrown, pos: 'bottom-left'});}
         
         });
    }
}
