$(document).on('submit','#updateItem',function( event ){
                   
                   event.preventDefault();
                   
                   var formData = $(this).serializeArray() ;
               
            
        
                    var dueDate = formData[3].value;
               
                if(formData[3].value.length>0) { dueDate += "T00:00:00Z";} else {dueDate = null;}
               // macOS  if(formData[3].value.length>0) { dueDate = toJSONLocal(new Date(formData[3].value));} else {dueDate = null;}
               
               console.log(dueDate);
               
                   var postData = JSON.stringify(
                                                 {
                                                 title:formData[0].value,
                                                 description:formData[1].value,
                                                 externalReference:formData[2].value,
                                                 dueDate:dueDate,
                                                 employeeId:Number(formData[4].value),
                                                 labels:formData[5].value,
                                                 type:formData[6].value,
                                                 status:formData[7].value,
                                                 userId:Number(formData[9].value),
                                                 order:Number(formData[10].value)
                                                 });
                   
                   var type = "post";
                   if ( formData[8].value > 0 ) {type = "patch"; }
               
                   mutateItem(postData, type, formData[8].value);
               
               
               
                   
                   });


$(document).on('submit','#newCommentForm',function( event ){
               
                   event.preventDefault();
                   
                   var formData = $(this).serializeArray() ;
                   
                   var postData = JSON.stringify(
                                                 {
                                                 comment:formData[0].value,
                                                 listItemId:Number(globallistItemId),
                                                 userId:0
                                                 
                                                 });
                   
                   $.ajax
                   ({
                    
                    url: '/comment',
                    type: 'post',
                    data: postData,
                    processData: false,
                    contentType: 'application/json',
                    success: function(result){ UIkit.notification(' Saved', {status: 'success', pos: 'bottom-left'});  $('#newComment').val(null); getComments(globallistItemId);},
                    error: function(XMLHttpRequest, textStatus, errorThrown){document.getElementById('showResult').innerHTML = '<div class="bs-callout bs-callout-danger">Error: '+ textStatus + ' '+errorThrown+'</div>';}
                    
                    })
                   
                   
 });





function mutateItem(postData, type, objectId) {
    
     var url = "/item";

    if ( type == "patch") {url = "/item/"+objectId;}
       
    $.ajax
    ({
     
     url: url,
     type: type,
     data: postData,
     processData: false,
     contentType: 'application/json',
     success: function(result){ UIkit.notification(' Saved', {status: 'success', pos: 'bottom-left'}); updateLists();},
     error: function(XMLHttpRequest, textStatus, errorThrown){
                let errorInfo=JSON.parse(XMLHttpRequest.responseText);
                UIkit.notification('Error: '+errorInfo.reason, {status: 'danger', pos: 'bottom-left'}); }
     
     });
    
}



$(document).on('submit','#editCommentForm',function( event ){
               
               console.log("Edit");
               
               event.preventDefault();
               
               var formData = $(this).serializeArray() ;
               
               var postData = JSON.stringify(
                                             {
                                             comment:formData[0].value,
                                             listItemId:Number(formData[1].value),
                                             userId:Number(formData[2].value),
                                             id:Number(formData[3].value)
                                             
                                             });
               
               $.ajax
               ({
                
                url: '/comment/'+formData[3].value,
                type: 'patch',
                data: postData,
                processData: false,
                contentType: 'application/json',
                success: function(result){ UIkit.notification(' Saved', {status: 'success', pos: 'bottom-left'});  selectItemOverlay(formData[1].value); },
                error: function(XMLHttpRequest, textStatus, errorThrown){UIkit.notification('Error: '+errorThrown, {status: 'danger', pos: 'bottom-left'});}
                
                })
               
               
 });



$(document).on('submit','#editColumnForm',function( event ){
               

               
               event.preventDefault();
               
               var formData = $(this).serializeArray() ;
               
               var postData = JSON.stringify(
                                             {
                                             id:Number(formData[0].value),
                                             column:Number(formData[1].value),
                                             name:formData[2].value,
                                             visibility:formData[3].value,
                                             userId:Number(formData[4].value)
                                             
                                             
                                             });
               var url = "/listcolumn";
               var type = "post";
               
               console.log(postData);
               
               if ( formData[0].value > 0 ) {url = url+"/"+Number(formData[0].value); type = "patch";}
               
               
               $.ajax
               ({
                
                url: url,
                type: type,
                data: postData,
                processData: false,
                contentType: 'application/json',
                success: function(result){ UIkit.notification(' Saved', {status: 'success', pos: 'bottom-left'}); location.reload(); },
                error: function(XMLHttpRequest, textStatus, errorThrown){UIkit.notification('Error: '+errorThrown, {status: 'danger', pos: 'bottom-left'});}
                
                })
               
               
               });




$(document).on('submit','#editListForm',function( event ){
               
               
               
               event.preventDefault();
               
               var formData = $(this).serializeArray() ;
               
               var postData = JSON.stringify(
                                             {
                                             id:Number(formData[0].value),
                                             typeCode:formData[1].value,
                                             name:formData[2].value,
                                             column:Number(formData[3].value),
                                             order:Number(formData[4].value),
                                             userId:Number(formData[5].value)
                                             
                                             
                                             });
               var url = "/listtype";
               var type = "post";
               
               console.log(postData);
               
               if ( formData[0].value > 0 ) {url = url+"/"+Number(formData[0].value); type = "patch";}
               
               
               $.ajax
               ({
                
                url: url,
                type: type,
                data: postData,
                processData: false,
                contentType: 'application/json',
                success: function(result){ UIkit.notification(' Saved', {status: 'success', pos: 'bottom-left'});  location.reload();},
                error: function(XMLHttpRequest, textStatus, errorThrown){UIkit.notification('Error: '+errorThrown, {status: 'danger', pos: 'bottom-left'});}
                
                })
               
               
               });



$(document).on('submit','#employeeForm',function( event ){
               
               event.preventDefault();
               
               var form = document.getElementById('employeeForm');
               var formData = new FormData(form);
               
               var formElements = $(this).serializeArray() ;
            
               var fileInput = document.getElementById('photoUpload');

               var file = fileInput.files[0];

               formData.append('picture', file);
               
               var url = "/employee";
               var type = "post";
               
              // console.log(formData);
               
               if ( formElements[0].value > 0 ) {url = url+"/"+Number(formElements[0].value);type = "post";} else {
                    formData.delete('id');
                    formData.delete('photo');
               }
               
               console.log(url);
               console.log(type);
               console.log(formElements[0].value);
               
               var xhr = new XMLHttpRequest();
               // Add any event handlers here...
               xhr.addEventListener('readystatechange', onreadystatechangeHandler, false);
               xhr.open(type, url, true);
               xhr.send(formData);

               
               });


function onreadystatechangeHandler(evt) {
    
     var status, text, readyState, statusText;
     try {
        readyState = evt.target.readyState;
        text = evt.target.responseText;
        status = evt.target.status;
        statusText = evt.target.statusText;
     }
     catch(e) {
        return;
    }
    
    if (readyState === 4) {
        if (status === 200) {
            // Success
            UIkit.notification(' Saved', {status: 'success', pos: 'bottom-left'});
            location.reload();
        } else {
            UIkit.notification('Error: '+statusText, {status: 'danger', pos: 'bottom-left'});
        }
    }
    
    
}





$(document).on('submit','#accountForm',function( event ){
               
               event.preventDefault();
               
               console.log("accountformPost");
               
               var form = document.getElementById('accountForm');
               var formData = new FormData(form);
               
               var formElements = $(this).serializeArray() ;
               
               var url = "/user/details";
               var type = "post";

               var xhr = new XMLHttpRequest();
               // Add any event handlers here...
               xhr.addEventListener('readystatechange', onreadystatechangeHandler, false);
               xhr.open(type, url, true);
               xhr.send(formData);
               
               
               });





$(document).on('submit','#register',function( event ){
               
               event.preventDefault();
               
               console.log("Register Requested");
               
               var formData = $(this).serializeArray() ;
               
               if (formData[1].value != formData[2].value ){
                  UIkit.notification('Error: Please ensure that both passwords match.', {status: 'danger', pos: 'bottom-left'});
                  return;
               }
               
               var postData = JSON.stringify(
                                             {
                                             email:formData[0].value,
                                             password:formData[1].value,
                                             code:formData[3].value
                                             
                                             });
               
               $.ajax
               ({
                
                url: '/register/invite',
                type: 'post',
                data: postData,
                processData: false,
                contentType: 'application/json',
                success: function(result){ UIkit.notification(' User Created', {status: 'success', pos: 'bottom-left'});
                                            window.location.href = "/login";
                },
                error: function(XMLHttpRequest, textStatus, errorThrown, responseText){
                        let errorInfo=JSON.parse(XMLHttpRequest.responseText);
                        UIkit.notification('Error: '+errorInfo.reason, {status: 'danger', pos: 'bottom-left'}); }
                
                })
               
               
               });



function toJSONLocal (date) {
    var local = new Date(date);
    local.setMinutes(date.getMinutes() - date.getTimezoneOffset());
    return local.toJSON().slice(0, 16)+".000Z";
}



$(document).on('submit','#changePassword',function( event ){
               
               
               
               event.preventDefault();
               
               var formData = $(this).serializeArray() ;
               
               if (formData[0].value != formData[1].value ){
                UIkit.notification('Error: Please ensure that both passwords match.', {status: 'danger', pos: 'bottom-left'});
                return;
               }
               
               
               var postData = JSON.stringify(
                                             {
                                             password:formData[0].value,
                                             accessKey:formData[2].value
                                             });
               var url = "/changepassword";
               var type = "post";

               $.ajax
               ({
                
                url: url,
                type: type,
                data: postData,
                processData: false,
                contentType: 'application/json',
                success: function(result){ UIkit.notification(' Saved', {status: 'success', pos: 'bottom-left'});
                    document.getElementById('showResult').innerHTML = '<div class="bs-callout bs-callout-success">Your password has been changed. Now you can <a href="/login">Login</a> again.</div>';
                
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    let errorInfo=JSON.parse(XMLHttpRequest.responseText);
                    UIkit.notification('Error: '+errorInfo.reason, {status: 'danger', pos: 'bottom-left'});}
                
                })
               
               
               });



$(document).on('submit','#changePasswordRequest',function( event ){
               
               
               
               event.preventDefault();
               
               var formData = $(this).serializeArray() ;
        
               if (formData[0].value.length < 10  ){
                    UIkit.notification('Error: Please enter a valid email address.', {status: 'danger', pos: 'bottom-left'});
               return;
               }
               
               var url = "/password/change/"+formData[0].value;
               var type = "get";
               
               $.ajax
               ({
                
                url: url,
                type: type,
                processData: false,
                contentType: 'application/json',
                success: function(result){ UIkit.notification(' Saved', {status: 'success', pos: 'bottom-left'});
                document.getElementById('showResult').innerHTML = '<div class="bs-callout bs-callout-success">Your password change request has been received. You will shortly receive an email with a link to click to change your password.</div>';
                
                },
                error: function(XMLHttpRequest, textStatus, errorThrown){
                    let errorInfo=JSON.parse(XMLHttpRequest.responseText);
                    UIkit.notification('Error: '+errorInfo.reason, {status: 'danger', pos: 'bottom-left'});}
                
                })
               
               
               });
